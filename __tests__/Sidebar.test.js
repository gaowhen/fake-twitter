import {render, screen, fireEvent} from '@testing-library/react'

import Sidebar from '@/components/Sidebar/Sidebar'

describe('Sidebar', () => {
  it('should renders a Home', async () => {
    render(<Sidebar />)
    expect(screen.queryByText('Home')).toBeInTheDocument()
  })

  it('should renders no Logout', async () => {
    render(<Sidebar />)
    expect(screen.queryByText('Log out')).toBeNull()
  })

  it('should return to homepage when clicking Home', async () => {
    render(<Sidebar />)
    const home = screen.queryByText('Home')
    fireEvent.click(home)
    expect(window.location.pathname).toBe('/')
  })
})
