import {render, screen, fireEvent, waitFor} from '@testing-library/react'
import {useRouter} from 'next/router'
import {useUser} from '../context/user'

import Signup from '@/pages/account/signup'

jest.mock('../context/user', () => ({
  useUser: jest.fn(),
}))

describe('Signup', () => {
  const mockSignup = jest.fn(() => Promise.resolve())

  beforeEach(() => {
    jest.clearAllMocks()

    useUser.mockReturnValue({
      user: null,
      signup: mockSignup,
    })
  })

  it('shoule renders the signup form', () => {
    render(<Signup />)

    expect(screen.getByLabelText('Username')).toBeInTheDocument()
    expect(screen.getByLabelText('Password')).toBeInTheDocument()
    expect(screen.getByLabelText('Confirm Password')).toBeInTheDocument()
    expect(
      screen.getByLabelText('I have read the agreement')
    ).toBeInTheDocument()
    expect(screen.getByRole('button', {name: 'Register'})).toBeInTheDocument()
  })

  it('should submits the form when all fields are filled out', async () => {
    render(<Signup />)

    fireEvent.change(screen.getByLabelText('Username'), {
      target: {value: 'testuser'},
    })
    fireEvent.change(screen.getByLabelText('Password'), {
      target: {value: 'password'},
    })
    fireEvent.change(screen.getByLabelText('Confirm Password'), {
      target: {value: 'password'},
    })
    await waitFor(async () => {
      fireEvent.click(screen.getByLabelText('I have read the agreement'))
    })
    await waitFor(async () => {
      fireEvent.click(screen.getByRole('button', {name: 'Register'}))
    })

    await waitFor(() => {
      expect(mockSignup).toHaveBeenCalledTimes(1)
      expect(mockSignup).toHaveBeenCalledWith({
        username: 'testuser',
        password: 'password',
        confirm: 'password',
        agreement: true,
      })

      expect(window.location.pathname === '/testuser')
    })
  })
})
