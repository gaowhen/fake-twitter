import {render, screen, fireEvent, waitFor} from '@testing-library/react'
import {useTheme} from 'next-themes'

import ThemeSwitch from '../components/ThemeSwitch'

jest.mock('next-themes')

let mockTheme = {
  resolvedTheme: 'light',
  setTheme: jest.fn().mockImplementation((newTheme) => {
    mockTheme.resolvedTheme = newTheme
  }),
}

describe('ThemeSwitch component', () => {
  beforeEach(() => {
    jest.clearAllMocks()

    useTheme.mockReturnValue(mockTheme)
  })

  afterEach(() => {
    mockTheme = {
      resolvedTheme: 'light',
      setTheme: jest.fn().mockImplementation((newTheme) => {
        mockTheme.resolvedTheme = newTheme
      }),
    }
  })

  it('renders correctly', () => {
    render(<ThemeSwitch />)

    const darkSwitch = screen.getByText('Dark Mod')

    expect(darkSwitch).toBeInTheDocument()
  })

  it('toggles theme when clicked', async () => {
    render(<ThemeSwitch />)

    const darkSwitch = screen.getByText('Dark Mod')

    fireEvent.click(darkSwitch)

    expect(useTheme().setTheme).toHaveBeenCalledWith('dark')
    // expect(darkSwitch).toHaveTextContent('Light Mod')

    // fireEvent.click(darkSwitch)
    // expect(useTheme().setTheme).toHaveBeenCalledWith('light')
  })
})
