import {useEffect, useState, useRef, useMemo} from 'react'
import {Spin} from 'antd'

import {useUser} from '../../context/user'
import useFeed from '../../hook/useFeed'

import Tweet from '../Tweet'
import TweetBox from '../TweetBox'

const Feed = () => {
  const {user} = useUser()
  const {ids, tweets, postTweet, deleteTweet, fetchTweets} = useFeed()

  const [showTweetBox, setShowTweetBox] = useState(false)
  const loadingRef = useRef(null)

  const shouldShowSpinner = useMemo(() => {
    return !tweets.length || (tweets.length && tweets.length !== ids.length)
  }, [tweets, ids])

  const shouldShowEnder = useMemo(() => {
    return tweets.length && tweets.length === ids.length
  }, [tweets, ids])

  // to prevent the Hydration failed error
  useEffect(() => {
    setShowTweetBox(typeof window !== 'undefined' && user)
  }, [user])

  useEffect(() => {
    if (!loadingRef.current) {
      return
    }

    const currentLoadingRef = loadingRef.current

    const observer = new IntersectionObserver((entries) => {
      const first = entries[0]

      if (first.isIntersecting) {
        fetchTweets(tweets.length, 10)
      }
    })

    observer.observe(currentLoadingRef)

    return () => {
      if (currentLoadingRef) {
        observer.unobserve(currentLoadingRef)
      }
    }
  }, [loadingRef, fetchTweets, tweets])

  return (
    <div className="min-h-screen col-span-9 md:col-span-7 border-solid border-y-0 border-gray-200 border-x dark:border-gray-800 pb-10">
      <div className="flex items-center justify-between">
        <h1 className="p-5 pb-0 text-xl font-bold">Home</h1>
      </div>

      {showTweetBox ? <TweetBox postTweet={postTweet} /> : null}

      <div>
        {tweets.map((tweet) => (
          <Tweet
            key={tweet.id}
            user={user}
            tweet={tweet}
            deleteTweet={deleteTweet}
          />
        ))}

        {shouldShowSpinner ? (
          <div
            className="m-4 flex justify-around items-center"
            ref={loadingRef}
          >
            <Spin />
          </div>
        ) : null}

        {shouldShowEnder ? (
          <div className="m-4 text-center"> - End - </div>
        ) : null}
      </div>
    </div>
  )
}

export default Feed
