import Link from 'next/link'

const Row = ({active, title, isHome}) => {
  return (
    <div
      className={
        active
          ? `group flex max-w-fit bg-blue-100
        cursor-pointer items-center space-x-2 rounded-full px-8 py-3
        hover:bg-blue-200 dark:bg-gray-600 mb-1 mt-1
        `
          : `group flex max-w-fit 
          cursor-pointer items-center space-x-2 rounded-full px-8 py-3
          hover:bg-gray-100 dark:hover:bg-gray-600 mb-1 mt-1`
      }
    >
      <p className="group-hover:text-twitter inline-flex text-base font-light m-0">
        {isHome ? <Link href="/">{title}</Link> : title}
      </p>
    </div>
  )
}

export default Row
