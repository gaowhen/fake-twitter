import Row from './Row'

export default {
  component: Row,
  title: 'Sidebar Row',
}

const Template = (args) => <Row {...args} />

export const Default = Template.bind({})
Default.args = {
  title: 'Home',
  active: false,
}

export const Active = Template.bind({})
Active.args = {
  title: 'Home Active',
  active: true,
}
