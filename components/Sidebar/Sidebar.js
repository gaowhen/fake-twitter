import {useState, useEffect} from 'react'
import {useRouter} from 'next/router'
import Image from 'next/image'
import Link from 'next/link'
import {Button} from 'antd'

import {useUser} from '../../context/user'

import Row from './Row'
import ThemeSwitch from '../ThemeSwitch'

import Logo from '../../assets/logo.svg'

const Sidebar = () => {
  const router = useRouter()
  const {user, logout} = useUser()
  const [showLogout, setShowLogout] = useState(false)

  useEffect(() => {
    if (typeof window !== 'undefined' && user && user.authed) {
      setShowLogout(true)
    }
  }, [user])

  const handleLogout = () => {
    logout()
    router.push('/account/signin')
  }

  return (
    <div className="sticky top-0 h-screen hidden col-span-2 md:flex flex-col justify-between item-center px-4 py-4 md:items-start">
      <div>
        <Link href="/">
          <Image className="w-10 h-10 mt-4" src={Logo} alt="twitter logo" />
        </Link>
        <Row title="Home" active isHome />
        <Row title="Explore" />
        <Row title="Notifications" />
        <Row title="Messages" />
        <Row title="Bookmarks" />
        <Row title="Twitter Blue" />
        <Row title="Profile" />
        <Row title="More" />
        <ThemeSwitch />
      </div>
      {showLogout ? (
        <div className="px-8">
          <Button type="link" onClick={handleLogout}>
            Log out
          </Button>
        </div>
      ) : null}
    </div>
  )
}

export default Sidebar
