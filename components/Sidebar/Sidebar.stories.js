import Sidebar from './Sidebar'

export default {
  component: Sidebar,
  title: 'Sidebar',
}

const Template = (args) => <Sidebar {...args} />

export const Default = Template.bind({})
Default.args = {}
