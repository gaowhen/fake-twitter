import {useRouter} from 'next/router'
import {CloseOutlined} from '@ant-design/icons'

import TimeAgo from 'react-timeago'

const Tweet = ({tweet, user, deleteTweet}) => {
  const router = useRouter()

  const handleNavigatePage = () => {
    router.push({
      pathname: `tweet/${tweet.id}`,
    })
  }

  const handleDelete = (e) => {
    e.stopPropagation()

    deleteTweet(tweet.id)

    // tweet detail page
    if (router.pathname === '/tweet/[id]') {
      router.back()
    }
  }

  return (
    <div className="border-solid border-x-0 border-y border-gray-100 p-5 hover:shadow-lg dark:border-gray-800 dark:hover:bg-gray-800">
      <div className="cursor-pointer" onClick={handleNavigatePage}>
        <div>
          <div className="flex items-center space-x-1">
            <p
              className={
                user?.username === tweet.author ? `font-bold` : `font-bold mr-1`
              }
            >
              {tweet.author}
            </p>
            <p className="hidden text-sm text-gray-500 sm:inline dark:text-gray-400">
              @{tweet.author.replace(/\s+/g, '')}
            </p>
            <TimeAgo
              className="text-sm text-gray-500 dark:text-gray-400"
              date={tweet.createdAt}
            />
          </div>
          <p className="pt-1">{tweet.text}</p>
        </div>
        {user?.username === tweet.author ? (
          <div className="flex justify-end">
            <CloseOutlined onClick={handleDelete} />
          </div>
        ) : null}
      </div>
    </div>
  )
}

export default Tweet
