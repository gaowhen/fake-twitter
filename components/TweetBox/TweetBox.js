import React, {useState} from 'react'
import {Input, Button, ConfigProvider} from 'antd'

import {useUser} from '../../context/user'

const {TextArea} = Input

function TweetBox({postTweet}) {
  const {user} = useUser()
  const [input, setInput] = useState('')

  const handleSubmit = (e) => {
    e.preventDefault()

    postTweet({
      author: user.username,
      text: input,
      createdAt: Date.now(),
    })

    setInput('')
  }

  return (
    <div className="flex space-x-2 p-5">
      <div className="flex flex-1 item-center pl-2">
        <form className="flex flex-1 flex-col">
          <TextArea
            rows={4}
            value={input}
            onChange={(e) => setInput(e.target.value)}
            type="text"
            placeholder="What's Happening"
            className="dark:bg-[#15202b] dark:text-white"
          />
          <div className="mt-4 flex items-center justify-end">
            <ConfigProvider
              theme={{
                components: {
                  Button: {
                    colorBgContainerDisabled: '#A6CDF6',
                    colorTextDisabled: '#fff',
                  },
                },
              }}
            >
              <Button type="primary" onClick={handleSubmit} disabled={!input}>
                Tweet
              </Button>
            </ConfigProvider>
          </div>
        </form>
      </div>
    </div>
  )
}

export default TweetBox
