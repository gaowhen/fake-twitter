import {useCallback, createContext, useContext, useReducer} from 'react'

import {useMutation} from '@tanstack/react-query'

import reducer, {ACTION} from '../reducer/account'

export const UserContext = createContext({
  state: {
    userMap: {},
  },
})

export const useUser = () => useContext(UserContext)

const UserProvider = ({children}) => {
  const [state, dispatch] = useReducer(
    reducer,
    typeof window !== 'undefined'
      ? JSON.parse(localStorage.getItem('user')) || {}
      : {}
  )

  const {mutate: signupMutate} = useMutation(({username, password}) => {
    dispatch({
      type: ACTION.SIGNUP,
      payload: {username, password},
    })
  })

  const {mutate: signinMutate} = useMutation(({username}) => {
    dispatch({
      type: ACTION.SIGNIN,
      payload: {username},
    })
  })

  const {mutate: logoutMutate} = useMutation(() => {
    dispatch({
      type: ACTION.LOGOUT,
    })
  })

  const signin = useCallback(
    ({username, password}) => {
      const pwd = state.accountMap[username]

      const authed = pwd && pwd === password ? true : false

      if (authed) {
        signinMutate({username})
      }

      return authed
    },
    [state, signinMutate]
  )

  const signup = useCallback(
    ({username, password}) => {
      if (state[username]) {
        return new Error('username exists')
      }

      signupMutate({
        username,
        password,
      })
    },
    [state, signupMutate]
  )

  const logout = useCallback(() => {
    logoutMutate()
  }, [logoutMutate])

  return (
    <UserContext.Provider
      value={{
        user: state.currentUser,
        signin,
        signup,
        logout,
      }}
    >
      {children}
    </UserContext.Provider>
  )
}

export default UserProvider
