import {useState, useEffect, useReducer, useCallback} from 'react'

import {useMutation} from '@tanstack/react-query'

import reducer, {ACTION} from '../reducer/feed'

const initialState = {tweetMap: {}, ids: [], nextId: 0}

const useFeed = () => {
  const [state, dispatch] = useReducer(reducer, initialState)

  useEffect(() => {
    try {
      const state = localStorage.getItem('feed')

      if (state) {
        dispatch({
          type: ACTION.INIT_STATE,
          payload: JSON.parse(state),
        })
      }
    } catch (e) {
      console.error(e)
    }
  }, [])

  const [tweets, setTweets] = useState([])

  const {mutate: postMutate} = useMutation((tweet) => {
    dispatch({
      type: ACTION.POST_TWEET,
      payload: tweet,
    })
  })

  const {mutate: deleteMutate} = useMutation((id) => {
    dispatch({
      type: ACTION.DELETE_TWEET,
      payload: {id},
    })
  })

  const postTweet = useCallback(
    (tweet) => {
      postMutate(tweet)
    },
    [postMutate]
  )

  const deleteTweet = useCallback(
    (id) => {
      deleteMutate(id)
    },
    [deleteMutate]
  )

  const getTweet = useCallback((id) => state.tweetMap[id], [state.tweetMap])

  const fetchTweets = useCallback(
    (start = 0, offset = 10) => {
      const end = start + offset

      const ids = state.ids.slice(start, end)

      setTweets((prev) => [...prev, ...ids.map((id) => state.tweetMap[id])])
    },
    [state]
  )

  useEffect(() => {
    setTweets((prev) => {
      const tweets = []

      // deal with deleted tweet
      prev.forEach((tweet) => {
        // false means the tweet has been deleted
        if (state.ids.includes(tweet.id)) {
          tweets.push(tweet)
        }
      })

      // trick
      // deal with newly created tweet
      if (state.ids.length && prev.length && state.ids[0] !== prev[0].id) {
        tweets.unshift(state.tweetMap[state.ids[0]])
      }

      return tweets
    })
  }, [state.ids, state.tweetMap])

  return {
    ids: state.ids,
    tweets,
    tweetMap: state.tweetMap,
    getTweet,
    postTweet,
    deleteTweet,
    fetchTweets,
  }
}

export default useFeed
