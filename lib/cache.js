import {QueryCache} from '@tanstack/react-query'

class LocalStorageCache {
  constructor() {
    this.queryCache = new QueryCache({
      defaultConfig: {
        queries: {
          cacheTime: Infinity,
        },
      },
    })
  }

  readQuery(queryKey) {
    const query = this.queryCache.getQuery(queryKey)

    if (query && query.state.data) {
      return query.state.data
    }

    return null
  }

  writeQuery(queryKey, data) {
    this.queryCache.setQueryData(queryKey, data)
    localStorage.setItem(queryKey.join(':'), JSON.stringify(data))
  }

  readQueries(queryKeys) {
    return queryKeys.map((queryKey) => this.readQuery(queryKey))
  }

  writeQueries(datas) {
    datas.forEach(([queryKey, data]) => this.writeQuery(queryKey, data))
  }
}

const cache = new LocalStorageCache()

export default cache
