import {useEffect} from 'react'
import {useRouter} from 'next/router'

import {useUser} from '../context/user'

import Feed from '../components/Feed'
import Layout from '../components/Layout'

export default function Home() {
  // const router = useRouter()
  // const {user} = useUser()

  // useEffect(() => {
  //   if (!user || !user.authed) {
  //     router.push('/account/signin')
  //   }
  // }, [user, router])

  return <Feed />
}

Home.getLayout = function getLayout(page) {
  return <Layout>{page}</Layout>
}
