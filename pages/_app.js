import {QueryClient, QueryClientProvider} from '@tanstack/react-query'
import {ReactQueryDevtools} from '@tanstack/react-query-devtools'
import {ThemeProvider} from 'next-themes'

import UserProvider from '../context/user'

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      staleTime: `Infinity`,
      cacheTime: `Infinity`,
    },
  },
})

import '../styles/globals.css'

function MyApp({Component, pageProps}) {
  const getLayout = Component.getLayout || ((page) => page)

  return (
    <ThemeProvider attribute="class">
      <QueryClientProvider client={queryClient}>
        <UserProvider>
          {getLayout(<Component {...pageProps} />)}
          <ReactQueryDevtools initialIsOpen={false} />
        </UserProvider>
      </QueryClientProvider>
    </ThemeProvider>
  )
}

export default MyApp
