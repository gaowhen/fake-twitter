import {useEffect} from 'react'
import Link from 'next/link'
import {useRouter} from 'next/router'
import {Row, Col, Form, Input, Checkbox, Button} from 'antd'
import {LockOutlined, UserOutlined} from '@ant-design/icons'

import {useUser} from '../../context/user'

const formItemLayout = {
  labelCol: {
    sm: {
      span: 8,
    },
  },
  wrapperCol: {
    sm: {
      span: 8,
    },
  },
}

const tailFormItemLayout = {
  wrapperCol: {
    sm: {
      span: 8,
      offset: 8,
    },
  },
}

export default function Signin() {
  const {user, signin} = useUser()

  const router = useRouter()

  useEffect(() => {
    if (user && user.authed) {
      router.push(`/${user.username}`)
    }
  }, [user, router])

  const onFinish = ({username, password}) => {
    const res = signin({username, password})
    console.log('Success:', res)
  }

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo)
  }

  return (
    <Row className="h-screen" justify="space-around" align="middle">
      <Col span={12}>
        <h1 className="text-center">Sign in</h1>
        <Form
          {...formItemLayout}
          name="normal_login"
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
        >
          <Form.Item
            name="username"
            label="Username"
            rules={[
              {
                required: true,
                message: 'Please input your Username!',
              },
            ]}
          >
            <Input prefix={<UserOutlined />} placeholder="Username" />
          </Form.Item>
          <Form.Item
            name="password"
            label="Password"
            rules={[
              {
                required: true,
                message: 'Please input your Password!',
              },
            ]}
          >
            <Input
              prefix={<LockOutlined />}
              type="password"
              placeholder="Password"
            />
          </Form.Item>

          <Form.Item {...tailFormItemLayout}>
            <Button block type="primary" htmlType="submit">
              Log in
            </Button>
            Or <Link href="/account/signup">register</Link>
          </Form.Item>
        </Form>
      </Col>
    </Row>
  )
}
