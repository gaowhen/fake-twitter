import {useEffect} from 'react'
import {useRouter} from 'next/router'
import {Row, Col, Button, Checkbox, Form, Input} from 'antd'

import {useUser} from '../../context/user'

const formItemLayout = {
  labelCol: {
    sm: {
      span: 8,
    },
  },
  wrapperCol: {
    sm: {
      span: 8,
    },
  },
}
const tailFormItemLayout = {
  wrapperCol: {
    sm: {
      span: 8,
      offset: 8,
    },
  },
}
export default function Singup() {
  const {user, signup} = useUser()
  const [form] = Form.useForm()

  const router = useRouter()

  useEffect(() => {
    if (user && user.authed) {
      router.push(`/${user.username}`)
    }
  }, [user, router])

  const onFinish = (values) => {
    console.log('Received values of form: ', values)
    signup(values)
    router.push(`/${values.username}`)
  }

  return (
    <Row className="h-screen" justify="space-around" align="middle">
      <Col span={12}>
        <h1 className="text-center">Sign up</h1>
        <Form
          {...formItemLayout}
          form={form}
          name="register"
          onFinish={onFinish}
          scrollToFirstError
        >
          <Form.Item
            name="username"
            label="Username"
            rules={[
              {
                required: true,
                message: 'Please input your nickname!',
                whitespace: true,
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            name="password"
            label="Password"
            rules={[
              {
                required: true,
                message: 'Please input your password!',
              },
            ]}
            hasFeedback
          >
            <Input.Password />
          </Form.Item>

          <Form.Item
            name="confirm"
            label="Confirm Password"
            dependencies={['password']}
            hasFeedback
            rules={[
              {
                required: true,
                message: 'Please confirm your password!',
              },
              ({getFieldValue}) => ({
                validator(_, value) {
                  if (!value || getFieldValue('password') === value) {
                    return Promise.resolve()
                  }
                  return Promise.reject(
                    new Error(
                      'The two passwords that you entered do not match!'
                    )
                  )
                },
              }),
            ]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item
            name="agreement"
            valuePropName="checked"
            rules={[
              {
                validator: (_, value) =>
                  value
                    ? Promise.resolve()
                    : Promise.reject(new Error('Should accept agreement')),
              },
            ]}
            {...tailFormItemLayout}
          >
            <Checkbox>
              I have read the <a href="">agreement</a>
            </Checkbox>
          </Form.Item>
          <Form.Item {...tailFormItemLayout}>
            <Button type="primary" htmlType="submit">
              Register
            </Button>
          </Form.Item>
        </Form>
      </Col>
    </Row>
  )
}
