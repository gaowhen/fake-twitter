import {useEffect} from 'react'
import {useRouter} from 'next/router'

import {useUser} from '../context/user'

export default function Home() {
  const router = useRouter()
  const {user} = useUser()

  useEffect(() => {
    router.push(`/${user.username}`)
  }, [user, router])

  return null
}
