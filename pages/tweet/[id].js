import {useEffect, useState} from 'react'
import {useRouter} from 'next/router'

import {useUser} from '../../context/user'
import useFeed from '../../hook/useFeed'

import Tweet from '../../components/Tweet'
import Layout from '../../components/Layout'

const Feed = () => {
  const router = useRouter()
  const {id} = router.query

  const {user} = useUser()
  const {tweetMap, deleteTweet} = useFeed()

  const [tweet, setTweet] = useState(null)

  useEffect(() => {
    if (id && tweetMap && tweetMap[id]) {
      setTweet(tweetMap[id])
    }
  }, [id, tweetMap])

  return (
    <div className="min-h-screen col-span-9 md:col-span-7 border-solid border-y-0 border-gray-200 border-x dark:border-gray-800">
      {tweet ? (
        <Tweet user={user} tweet={tweet} deleteTweet={deleteTweet} />
      ) : null}
    </div>
  )
}

Feed.getLayout = function getLayout(page) {
  return <Layout>{page}</Layout>
}

export default Feed
