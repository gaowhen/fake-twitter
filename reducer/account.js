export const ACTION = {
  SIGNIN: 'SIGNIN',
  SIGNUP: 'SIGNUP',
  LOGOUT: 'LOGOUT',
}

const reducer = (
  state = {
    currentUser: null,
    accountMap: {},
  },
  {type, payload}
) => {
  console.log(`reducer ${type} ${JSON.stringify(payload, null, 2)}`)

  switch (type) {
    case ACTION.SIGNIN:
      const {username} = payload

      state = {
        ...state,
        currentUser: {
          username,
          authed: true,
        },
      }

      localStorage.setItem('user', JSON.stringify(state))

      return state
    case ACTION.SIGNUP: {
      const {username, password} = payload

      state = {
        ...state,
        accountMap: {
          ...state.accountMap,
          [username]: password,
        },
      }

      localStorage.setItem('user', JSON.stringify(state))

      return state
    }
    case ACTION.LOGOUT:
      state = {
        ...state,
        currentUser: null,
      }

      localStorage.setItem('user', JSON.stringify(state))

      return state
    default:
      return state
  }
}

export default reducer
