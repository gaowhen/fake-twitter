export const ACTION = {
  INIT_STATE: 'INIT_STATE',
  POST_TWEET: 'POST_TWEET',
  DELETE_TWEET: 'DELETE_TWEET',
}

const reducer = (
  state = {
    tweetMap: {},
    ids: [],
    nextId: 0,
  },
  {type, payload}
) => {
  console.log(`reducer ${type} ${JSON.stringify(payload, null, 2)}`)

  switch (type) {
    case ACTION.INIT_STATE:
      state = payload

      return state
    case ACTION.POST_TWEET:
      const nextId = state.nextId + 1

      state = {
        tweetMap: {
          ...state.tweetMap,
          [nextId]: {...payload, id: nextId},
        },
        ids: [nextId, ...state.ids],
        nextId,
      }

      localStorage.setItem('feed', JSON.stringify(state))

      return state
    case ACTION.DELETE_TWEET:
      const {id} = payload
      const tweetMap = state.tweetMap

      delete tweetMap[id]

      state = {
        tweetMap,
        ids: state.ids.filter((item) => item !== id),
        nextId: state.nextId,
      }

      localStorage.setItem('feed', JSON.stringify(state))
      return state
    default:
      return state
  }
}

export default reducer
